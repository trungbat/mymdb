from django.shortcuts import render

from django.views.generic import ListView, DetailView
from .models import MovieModel

class MovieListView(ListView):
	model = MovieModel
	template_name = 'core/movie_list.html'


class MovieDetailView(DetailView):
	model = MovieModel
	template_name = 'core/movie_detail.html'
	