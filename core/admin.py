from django.contrib import admin
from .models import MovieModel, Person

admin.site.register(MovieModel)
admin.site.register(Person)