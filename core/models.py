from django.db import models

class MovieModel(models.Model):
	NOT_RATED = 0
	RATED_G = 1
	RATED_PG  = 2
	RATED_R = 3
	RATINGS = (
		(NOT_RATED, 'NR - Not Rated'),
		(RATED_G,'G - General Audiences'),
		(RATED_PG, 'PG - Parental Guidance'),
		(RATED_R, 'R - Restricted'),)

	title = models.CharField(('Tiêu đề phim'), max_length = 140)
	#relationship
	director = models.ForeignKey(
		to = 'Person',
		on_delete=  models.SET_NULL,
		related_name  = 'ditected',
		null = True,
		blank = True,
	)
	writers = models.ManyToManyField(
		to = 'Person',
		related_name = 'writing_credits',
		blank= True,
	)
	actors=  models.ManyToManyField(
		to = 'Person',
		through = 'Role',
		related_name  = 'acting_credits',
		blank = True,
	)

	plot = models.TextField(('Giới thiệu'),)
	year = models.PositiveIntegerField(('Năm'),)
	rating = models.IntegerField(('Xếp hạng'),choices = RATINGS, default = NOT_RATED)
	runtime = models.PositiveIntegerField(('Thời lượng'),)
	website = models.URLField(('URL Website'),blank = True)

	def __str__(self):
		return '{} ({})'.format(self.title,self.year)

	class Meta:
		ordering = ('-year', 'title')
		verbose_name_plural = 'Movies'


class Person(models.Model):
	first_name = models.CharField(('Họ'),max_length = 100)
	last_name = models.CharField(('Tên'),max_length = 100)
	born = models.DateField(('Ngày sinh'),)
	died =models.DateField(('Ngày mất'), null = True,blank = True)

	class Meta:
		ordering = ('last_name','first_name')
		verbose_name_plural = "Person"

	def __str__(self):
		if self.died:
			return '{} {} ({}-{})'.format(
				self.first_name,
				self.last_name,
				self.born,
				self.died,
			)
		return '{} {} ({})'.format(
				self.first_name,
				self.last_name,
				self.born,
			)



class Role(models.Model):
	movie = models.ForeignKey(MovieModel, on_delete = models.DO_NOTHING)
	person = models.ForeignKey(Person, on_delete = models.DO_NOTHING)
	name = models.CharField(max_length = 140)

	def __str__(self):
		return '{} {} {}'.format(self.movie_id, self.person_id, self.name)

	class Meta:
		unique_together = ('movie','person','name')
